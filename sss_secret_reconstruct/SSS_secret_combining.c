/*
 * A compact implementation for the SSS algorithm
 */
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "shamir.h"

int main(int argc, char *argv[])
{
/*
u_int8_t essential_shares = 5;
printf("Threshold = %hhu\n", essential_shares);

u_int8_t total_shares = 11;
printf("Total shares = %hhu\n", total_shares);

char secret[32] = "Test_secret";
printf("Main secret for splitting = %s\n", secret);

char * keys;
u_int8_t secret_size = strlen(secret);
keys = encrypt(secret, secret_size, essential_shares, total_shares);
printf("SHARES = \n%s", keys);
*/


char *  test_shares[] = {"66c0c4e8fcfd32c6999638c8","377ca5951d45d6c460e4fd2f","d63b56df07e50bc1c2f3ec79", "60b67b90e05b5330c17c0856", "136e1cab6cc0b44bf4d2efb3"};

int essential_shares = 5;
int secret_size = strlen(test_shares[2]) - 1;
u_int8_t **shares = (u_int8_t **) malloc(essential_shares * sizeof(u_int8_t *));

printf("USED_SHARES = \n");

for (int i=0; i < essential_shares; i++)
	printf("%s\n", test_shares[i]);


for (int i=0; i < essential_shares; i++)
    shares[i] = hex_str_to_arr(test_shares[i]);

u_int8_t *message = decrypt(shares, secret_size, essential_shares);

printf("\nDecoded String: %s\n", (char *) message);



/*
int essential_shares2 = 3;

char test_keys[200] = "014c4054091cec7ff9c52192 608499387fc80d31534d2ecb 13ea25a5398953b24b099c5b";

int secret_size2 = strlen(test_keys) - 1;

//char* test1 = &test_keys;

u_int8_t **shares = (u_int8_t **) malloc(essential_shares2 * sizeof(u_int8_t *));

for (int i=0; i < essential_shares2; i++)
	shares[i] = hex_str_to_arr((char *) test_keys[i]);

printf("\nDecoded String: %s\n", shares);

u_int8_t *message = decrypt(shares, secret_size2, essential_shares2);

printf("\nDecoded String: %s\n", (char *) message);
*/

/* Consol version of SSS program, don't work at the moment
    // Passed argument is encryption
    if (strcmp(argv[1], "encrypt") == 0)
    {
        srand((unsigned) time(NULL));
        u_int8_t essential_shares = 2;
        u_int8_t total_shares = 4;
        char secret[32];
        char * keys;

        printf("Enter string: ");

        fgets(secret, sizeof secret, stdin);
        u_int8_t secret_size = strlen(secret);

        keys = encrypt(secret, secret_size, essential_shares, total_shares);
        printf("KEYS = \n%s", keys);

    }

    // Passed argument is decryption
    else if (strcmp(argv[1], "decrypt") == 0)
    {
        int essential_shares = argc - 2;
        int secret_size = strlen(argv[2]) - 1;
        u_int8_t **shares = (u_int8_t **) malloc(essential_shares * sizeof(u_int8_t *));

        for (int i=0; i < essential_shares; i++)
            shares[i] = hex_str_to_arr(argv[i + 2]);

        u_int8_t *message = decrypt(shares, secret_size, essential_shares);

        printf("\nDecoded String: %s\n", (char *) message);
    }

    // Default argument
    else {
        printf(
                "Shamir Secret Sharing Scheme Implementation: SSS\n\n"
                "1. Split a code in 4 shares, so 2 are needed to reconstruct it:\n"
                "$ SSS encrypt\n\n"
                "2. Join k shares to decrypt the original message::\n"
                "$ SSS decrypt SHARE_1 SHARE_2 ... SHARE_K\n\n"
        );
    }
    */

    return 0;
}
