import random
from decimal import Decimal
import argparse

FIELD_SIZE = 10 ** 5


def reconstruct_secret(shares):
    """
    В данной функции происходит восстановление общего секрета
    из отдельных шардов (shares). Восстановление происходит с помощью
    формул интерполяции Лагранджа

    Подаваемые на вход данные - это точки (x_y), принадлежащие полиному,
    в котором свободный член является секретным ключом.

    Формула интерполяции работает для целочисленных секрета и коэффициентов,
     содержащих не более 28 цифр (точность округления в decimal)
    """
    sums = 0

    reformat_shares = []
    for item in shares:
        reformat_shares.append((int(item.split("_")[0]), int(item.split("_")[1])))

    for j, share_j in enumerate(reformat_shares):
        xj, yj = share_j
        prod = Decimal(1)

        for i, share_i in enumerate(reformat_shares):
            xi, _ = share_i
            if i != j:
                prod *= Decimal(Decimal(xi) / (xi - xj))

        prod *= yj
        sums += Decimal(prod)
    return int(round(Decimal(sums), 0))


def polynom(x, coefficients):
    point = 0
    for coefficient_index, coefficient_value in enumerate(coefficients[::-1]):
        point += x ** coefficient_index * coefficient_value
    return point


def coeff(t, secret):
    coeff = [random.randrange(0, FIELD_SIZE) for _ in range(t - 1)]
    coeff.append(secret)
    return coeff


def generate_shares(n, m, secret):
    coefficients = coeff(m, secret)
    shares = []

    for i in range(1, n + 1):
        x = random.randrange(1, FIELD_SIZE)
        shares.append(str(x)+"_" + str(polynom(x, coefficients)))

    return shares


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parameters for secret sharing')
    parser.add_argument(
        "--secret",
        type=str,
        help='Secret value in hex format. Required parameter for key sharing process')
    parser.add_argument(
        "--direction",
        type=int,
        help='Direction of transformation. Specify 0 if you want to share the secret.'
             ' Specify 1 if you need to combine a secret.'
             ' This parameter is required for both sharing and combining the secret')
    parser.add_argument(
        "--threshold",
        type=int,
        help='Threshold value. Required parameter for key sharing procedure. Type - integer')
    parser.add_argument(
        "--total",
        type=int,
        help='Total number of secret parts(shares). Required parameter for key sharing procedure. Type - integer')
    parser.add_argument(
        "--shares",
        type=str,
        nargs='*',
        help='Indicate, separated by a space,'
             ' the values of the shares from which you need to combine the secret.'
             ' Required parameter for key assembly')
    args = parser.parse_args()

    if args.secret and args.threshold and args.total and args.direction == 0:
        int_input = int(args.secret, base=16)
        print("Integer value corresponding to the entered key", int_input)
        if len(str(int_input)) >= 27:
            print("The current version of the program works "
                  "with coefficients and values of no more than 27 characters")
        else:
            output_shares = generate_shares(args.total, args.threshold, int_input)
            print("List of shares:")
            for one_share in output_shares:
                print(one_share)
    elif args.shares and args.direction == 1:
        recovered_secret = reconstruct_secret(args.shares)
        print("Recovered integer representation of secret", recovered_secret)
        print("Recovered secret in hex format", hex(recovered_secret))
    else:
        print("Invalid set of input parameters specified. Use the help")
